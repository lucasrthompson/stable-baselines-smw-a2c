import retro
from stable_baselines import TD3, SAC, ACER, PPO2, DQN, A2C
from stable_baselines.common.policies import MlpPolicy, CnnPolicy, MlpLstmPolicy,CnnLstmPolicy


env = retro.make("SuperMarioWorld-Snes", 'DonutPlains1', record='.')


model = A2C(CnnPolicy,  env, n_steps=128, verbose=1) 

model.learn(total_timesteps=250000)

model.save("smw-a2c")
  

# once finished training, watch results

obs = env.reset()
done = False
reward = 0
while not done:
  action, _ = model.predict(obs)
  #print(action, env.action_space.sample())
  obs, rew, done, info = env.step(action)
  env.render()
  reward += rew
  
  print(reward)
